import fs from 'fs';
import {
  Tabletojson
} from 'tabletojson';
import * as moment from 'moment';
import { cloneDeep } from 'lodash';

const DAY_COUNT_MULTIPLIER = 10;
const HOURS_MULTIPLIER = 5;
const EVEN_MULTIPLIER = 3;
const DAYS_MULTIPLIER: Record<number, number> = {
  0: 0,
  1: 5,
  2: 5,
  3: 5,
  4: 0,
  5: 0,
  6: 0,
};

interface RawSubject {
  id: string;
  Code: string;
  Name: string;
  Group: string;
  Type: string;
  Day: string;
  From: string;
  To: string;
  'Class Size': string;
  Enrolled: string;
  Waiting: string;
  Status: string;
  Location: string;
}

interface Subject {
  id: number;
  code: string;
  name: string;
  group: number;
  type: CourseSessionTypes;
  day: string;
  from: string;
  to: string;
  size: number;
  enrolled: number;
  waiting: number;
  status: string;
  location: string;
}

interface CourseSession {
  weekDay: number;
  startTime: string;
  endTime: string;
  location: string;
}

interface Course {
  code: string;
  title: string;
  lecture: CourseSession;
  tutorial?: CourseSession;
}

enum SubjectTypes {
  LEC_TUT_STRICT,
  LEC_TUT,
  LEC,
}

enum CourseSessionTypes {
  TUTORIAL = 'Tutorial',
  LECTURE = 'Lecture',
}

const groupBy = <
  T extends Record<string, T[keyof T]>,
  K extends keyof T,
  >(xs: T[], key: K) => {
  return xs.reduce<T[K] extends string | number ? Record<T[K], T[]> : any>(function (prevVal, currVal) {
    const currKey = currVal[key];
    if (typeof currKey === 'string' || typeof currKey === 'number') {
      ((prevVal[currKey] = prevVal[currKey] || []) as any).push(currVal);
    }
    return prevVal;
  }, {} as any);
};

const html = fs.readFileSync('./table.html').toString();

const tableJson = Tabletojson.convert(html, {
  stripHtmlFromCells: false,
});

const keys = Object.keys(tableJson);
const newKeys = [
  'id',
  'code',
  'name',
  'group',
  'type',
  'day',
  'from',
  'to',
  'size',
  'enrolled',
  'waiting',
  'status',
  'location',
]
const fixTime = (time: string) => {
  let [hours, minutes] = time.split(':').map((a) => parseInt(a));
  if (hours < 8) {
    hours += 12;
  }
  return `0${hours}`.slice(-2) + ':' + `0${minutes}`.slice(-2);
}

const detailedSubjects = {
  MTHN103: {
    code: 'MTHN103',
    type: SubjectTypes.LEC_TUT_STRICT,
    hours: 3,
  },
  CMPN102: {
    code: 'CMPN102',
    type: SubjectTypes.LEC_TUT,
    hours: 3,
  },
  CMPN103: {
    code: 'CMPN103',
    type: SubjectTypes.LEC_TUT,
    hours: 3,
  },
  // CMPN201: {
  //   code: 'CMPN201',
  //   type: SubjectTypes.LEC_TUT,
  //   hours: 3,
  // },
  ELCN102: {
    code: 'ELCN102',
    type: SubjectTypes.LEC_TUT,
    hours: 3,
  },
  INTN125: {
    code: 'INTN125',
    type: SubjectTypes.LEC_TUT_STRICT,
    hours: 3,
  },
  GENN102: {
    code: 'GENN102',
    type: SubjectTypes.LEC,
    hours: 2,
  },
  // ELCN100: {
  //   code: 'ELCN100',
  //   type: SubjectTypes.LEC,
  //   hours: 2,
  // },
  MTHN102: {
    code: 'MTHN102',
    type: SubjectTypes.LEC_TUT_STRICT,
    hours: 3,
  },
};

const targetSubjects = Object.keys(detailedSubjects);

const isInTargetSubjects = (subject: string) => {
  let inTargetSubjects = false;
  targetSubjects.some((targetSubject) => {
    if (subject.includes(targetSubject)) {
      inTargetSubjects = true;
      return true;
    }
  });
  return inTargetSubjects;
}

const filteredData = (tableJson[0] as RawSubject[])
  .filter(
    (row) => isInTargetSubjects(row.Code)
      && row.Status === '___Opened___'
  )
  .sort((rowA, rowB) => {
    const indexA = targetSubjects.indexOf(rowA.Code.replace(/_/g, ''));
    const indexB = targetSubjects.indexOf(rowB.Code.replace(/_/g, ''));
    return indexA - indexB;
  })
  .map((row) => ({
    id: parseInt(row.id),
    code: row.Code.replace(/_/g, ''),
    name: row.Name,
    group: parseInt(row.Group),
    type: row.Type.replace(/_/g, ''),
    day: row.Day,
    from: fixTime(row.From.replace(/_/g, '')),
    to: fixTime(row.To.replace(/_/g, '')),
    size: parseInt(row['Class Size']),
    enrolled: parseInt(row.Enrolled),
    waiting: parseInt(row.Waiting),
    status: row.Status.replace(/_/g, ''),
    location: row.Location.replace(' - &#x627;&#x644;&#x62C;&#x64A;&#x632;&#x629; &#x627;&#x644;&#x631;&#x626;&#x64A;&#x633;&#x64A;', ''),
  }));

const groupedBySubject = groupBy(filteredData, 'code');
const courses = Object.entries(groupedBySubject).map(([subjectKey, subjectValue]) => {
  if ((detailedSubjects as any)[subjectKey].type === SubjectTypes.LEC_TUT_STRICT) {
    const groupedByGroup = groupBy(subjectValue, 'group');
    return Object
      .entries(groupedByGroup)
      .filter(([, groupValue]) => groupValue.length === 2)
      .map<Course>(
        ([, groupValue]) => {
          const tutorial = groupValue.find((a) => a.type === CourseSessionTypes.TUTORIAL) as Subject;
          const lecture = groupValue.find((a) => a.type === CourseSessionTypes.LECTURE) as Subject;
          return ({
            code: groupValue[0].code,
            title: groupValue[0].name,
            lecture: {
              location: lecture.location,
              startTime: lecture.from,
              endTime: lecture.to,
              weekDay: moment.weekdays().indexOf(lecture.day as string),
            },
            tutorial: {
              location: tutorial.location,
              startTime: tutorial.from,
              endTime: tutorial.to,
              weekDay: moment.weekdays().indexOf(tutorial.day as string),
            },
          });
        }
      );
  } else if ((detailedSubjects as any)[subjectKey].type === SubjectTypes.LEC_TUT) {
    const lectures = subjectValue.filter((subject) => subject.type === CourseSessionTypes.LECTURE);
    const tutorials = subjectValue.filter((subject) => subject.type === CourseSessionTypes.TUTORIAL);
    const combinations: Course[] = [];
    lectures.forEach((lecture) => {
      tutorials.forEach((tutorial) => {
        combinations.push({
          code: lecture.code,
          title: lecture.name,
          lecture: {
            location: lecture.location,
            startTime: lecture.from,
            endTime: lecture.to,
            weekDay: moment.weekdays().indexOf(lecture.day as string),
          },
          tutorial: {
            location: tutorial.location,
            startTime: tutorial.from,
            endTime: tutorial.to,
            weekDay: moment.weekdays().indexOf(tutorial.day as string),
          },
        });
      });
    });
    return combinations;
  } else {
    return subjectValue
      .map<Course>((subject) => ({
        code: subject.code,
        title: subject.name,
        lecture: {
          location: subject.location,
          startTime: subject.from,
          endTime: subject.to,
          weekDay: moment.weekdays().indexOf(subject.day as string),
        },
      }));
  }
});

fs.writeFileSync('./dist/courses.json', JSON.stringify(courses, undefined, 2));

let schedules: Record<string, Course>[] = [];

const flattenCourses = (courses: Course[]) => {
  const flattenedSchedule: CourseSession[] = [];
  Object.values(courses).forEach((s) => {
    flattenedSchedule.push(s.lecture);
    if (s.tutorial) {
      flattenedSchedule.push(s.tutorial);
    }
  });
  return flattenedSchedule;
}

const interferesWithSchedule = (courses: Record<string, Course>, course: Course) => {
  let interferes = false;
  const flattenedSchedule: CourseSession[] = flattenCourses(Object.values(courses));
  const flattenedCourse: CourseSession[] = flattenCourses([course]);
  flattenedSchedule.some((scheduleCourseSession) => {
    flattenedCourse.some((courseSession) => {
      if (
        courseSession.weekDay === scheduleCourseSession.weekDay
        && courseSession.startTime < scheduleCourseSession.endTime
        && courseSession.endTime > scheduleCourseSession.startTime
      ) {
        interferes = true;
      }
      return interferes;
    });
    return interferes;
  });
  return interferes;
}

const getSubjects = (courses: Course[][], currentSubjectIndex: number, schedule: Record<string, Course>) => {
  if (!courses[currentSubjectIndex] || !courses[currentSubjectIndex].length) {
    if (currentSubjectIndex === courses.length - 1) {
      schedules.push(schedule);
    } else {
      getSubjects(courses, currentSubjectIndex + 1, cloneDeep(schedule));
    }
  }
  courses[currentSubjectIndex]
    .map(cloneDeep)
    .forEach((course) => {
      let newSchedule = cloneDeep(schedule);
      if (!interferesWithSchedule(schedule, course)) {
        newSchedule = {
          ...newSchedule,
          [course.code]: course,
        };
      }
      if (currentSubjectIndex === courses.length - 1) {
        schedules.push(newSchedule);
      } else {
        getSubjects(courses, currentSubjectIndex + 1, cloneDeep(newSchedule));
      }
    });
}

getSubjects(courses, 0, {});

schedules = schedules.sort((a, b) => Object.keys(b).length - Object.keys(a).length);

const highestLength = Object.keys(schedules[0]).length;

schedules = schedules.filter((a) => Object.keys(a).length === highestLength);

const getDayCount = (schedule: Record<string, Course>) => {
  const days = new Set<number>();
  Object.values(schedule).forEach((course) => {
    days.add(course.lecture.weekDay);
    if (course.tutorial) {
      days.add(course.tutorial.weekDay);
    }
  });
  return days.size;
};

const getDaysScore = (schedule: Record<string, Course>) => {
  const dayData: Record<number, { minStartTime: string; maxEndTime: string }> = {};
  Object
    .values(schedule)
    .map((course) => flattenCourses([course]))
    .forEach((courseSessions) => {
      courseSessions.forEach((courseSession) => {
        const {
          weekDay,
          startTime,
          endTime,
        } = courseSession;
        if (!dayData[weekDay]) {
          dayData[weekDay] = {
            minStartTime: startTime,
            maxEndTime: endTime,
          };
        } else {
          if (startTime < dayData[weekDay].minStartTime) {
            dayData[weekDay].minStartTime = startTime;
          }
          if (endTime > dayData[weekDay].maxEndTime) {
            dayData[weekDay].maxEndTime = endTime;
          }
        }
      });
    });
  let score = 0;
  Object.entries(dayData).forEach(([key, day]) => {
    const [fromHours, fromMinutes] = day.minStartTime.split(':').map(a => parseInt(a));
    const [toHours, toMinutes] = day.maxEndTime.split(':').map(a => parseInt(a));
    score += (fromHours + moment.duration({
      from: moment.unix(0).add(fromHours, 'hours').add(fromMinutes, 'minutes'),
      to: moment.unix(0).add(toHours, 'hours').add(toMinutes, 'minutes'),
    }).asHours()) * DAYS_MULTIPLIER[parseInt(key)];
  });
  return score;
};

const getEvenScore = (schedule: Record<string, Course>) => {
  let score = 0;
  Object
    .values(schedule)
    .map((a) => flattenCourses([a]))
    .forEach((a) => {
      a.forEach((b) => {
        if (b.location.includes('odd')) {
          score += 1;
        }
      });
    });
  return score;
};

const getScore = (schedule: Record<string, Course>) => {
  const dayCountStore = getDayCount(schedule) * DAY_COUNT_MULTIPLIER;
  const hoursStore = getDaysScore(schedule) * HOURS_MULTIPLIER;
  const evenScore = getEvenScore(schedule) * EVEN_MULTIPLIER;
  return dayCountStore + hoursStore + evenScore;
}

schedules = schedules.sort((a, b) => getScore(a) - getScore(b));

const lowestScore = getScore(schedules[0]);

schedules = schedules.filter((a) => getScore(a) === lowestScore);

fs.writeFileSync('./dist/schedules.json', JSON.stringify(schedules, undefined, 2));

// ========


interface AlarmData {
  duration: string;
}

const createAlarm = (alarmData: AlarmData) => {
  return `BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:This is a reminder
TRIGGER:-P${alarmData.duration}
END:VALARM
`;
}

interface EventData {
  startDate: string;
  endDate: string;
  weekDay: string;
  title: string;
  alarms: AlarmData[];
}

const createEvent = (eventData: EventData) => {
  let alarms = '';
  eventData.alarms.forEach((alarm) => alarms += createAlarm(alarm));
  return `BEGIN:VEVENT
DTSTART;TZID=Africa/Cairo:${eventData.startDate}
DTEND;TZID=Africa/Cairo:${eventData.endDate}
RRULE:FREQ=WEEKLY;BYDAY=${eventData.weekDay}
DESCRIPTION:
LOCATION:Faculty of Engineering - Cairo University\, Cairo University Rd\, 
  Oula\, Giza District\, Giza Governorate\, Egypt
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:${eventData.title}
TRANSP:OPAQUE
${alarms}
END:VEVENT
`;
};

const createEventFromCourse = (code: string, course: Course) => {
  return createEventFromCourseSession(code, course.title, course.lecture) + createEventFromCourseSession(code, course.title, course.tutorial, false);
};

const createEventFromCourseSession = (code: string, title: string, courseSession?: CourseSession, isLecture = true) => {
  if (courseSession) {
    const currentDate = new Date();
    const startOfWeek = moment.default(currentDate).startOf('week');
    const startOfDay = startOfWeek.clone().add(courseSession.weekDay, 'days');
    const startTime = startOfDay.clone().add(moment.duration(courseSession.startTime).hours(), 'hours').add(moment.duration(courseSession.startTime).minutes(), 'minutes').format('YYYYMMDDTHHmmss');
    const endTime = startOfDay.clone().add(moment.duration(courseSession.endTime).hours(), 'hours').add(moment.duration(courseSession.endTime).minutes(), 'minutes').format('YYYYMMDDTHHmmss');
    const eventData: EventData = {
      alarms: [
        {
          duration: '0DT0H30M0S',
        },
        {
          duration: '0DT1H0M0S',
        },
      ],
      endDate: endTime,
      startDate: startTime,
      title: `[${code}][${courseSession.location}] ${isLecture ? 'Lecture of' : 'Tutorial of'} ${title}`,
      weekDay: moment.weekdaysMin()[courseSession.weekDay].toUpperCase(),
    };
    return createEvent(eventData);
  }
  return '';
};

if (fs.existsSync('./schedules/')) {
  fs.rmdirSync('./schedules', {
    recursive: true,
  });
}
fs.mkdirSync('./schedules/');

Object.values(schedules).forEach((schedule, index) => {
  let icsFile = `BEGIN:VCALENDAR
PRODID:-//Georges DIMITRY//College
VERSION:2.0
`;
  Object
    .entries(schedule)
    .forEach(([code, value]) => {
      icsFile += createEventFromCourse(code, value);
    });
  icsFile += `END:VCALENDAR`;
  fs.writeFileSync(`./schedules/schedule${index}.ics`, icsFile);
});
